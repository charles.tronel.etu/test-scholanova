package game.modele;

public enum Operateur {
    ADDITION ("+"),
    SOUSTRACTION ("-"),
    MULTIPLICATION ("*"),
    DIVISION ("/");

    String symbole;

    Operateur (String symbole) {
        this.symbole = symbole;
    }

    public String getSymbole () {
        return this.symbole;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("Operateur : ")
                .append(this.symbole)
                .toString();
    }
}

package game.modele;

import java.util.Random;

public class Operation {
    private Nombre nombre1;
    private Nombre nombre2;
    private Operateur operateur;

    private Double resultat;

    private Random random = new Random();

    public Operation(int maxNombre) {
        this.nombre1 = new Nombre(maxNombre);
        this.nombre2 = new Nombre(maxNombre);
        this.operateur = Operateur.values()[random.nextInt(Operateur.values().length)];

        if (operateur.equals(Operateur.DIVISION)) {
            while (this.nombre2.getNombre().equals(0)) {
                this.nombre2.generer();
            }
        }
    }

    public void resultat() {
        this.resultat = Double.valueOf(-999999999);

        switch (operateur) {
            case ADDITION:
                this.resultat = Double.valueOf(this.nombre1.getNombre() + this.nombre2.getNombre());
                break;
            case SOUSTRACTION:
                this.resultat = Double.valueOf(this.nombre1.getNombre() - this.nombre2.getNombre());
                break;
            case MULTIPLICATION:
                this.resultat = Double.valueOf(this.nombre1.getNombre() * this.nombre2.getNombre());
                break;
            case DIVISION:
                this.resultat = Double.valueOf(this.nombre1.getNombre() / this.nombre2.getNombre());
                break;
        }
    }

    public Double getResultat() {
        return this.resultat;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append(this.nombre1.getNombre()).append(" ")
                .append(this.operateur.getSymbole()).append(" ")
                .append(this.nombre2.getNombre())
                .append(" = ?")
                .toString();
    }
}

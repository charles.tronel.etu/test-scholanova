package game.modele;

import java.util.Random;

public class Nombre {
    private Integer nombre;
    private int maxNombre;

    private Random random = new Random();

    public Nombre (int maxNombre) {
        this.maxNombre = maxNombre+1;
        this.nombre = random.nextInt(this.maxNombre);
    }

    public void generer () {
        this.nombre = this.random.nextInt(this.maxNombre);
    }

    public Integer getNombre() {
        return nombre;
    }

    public void setNombre(Integer nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("Nombre : ")
                .append(this.nombre)
                .toString();
    }
}
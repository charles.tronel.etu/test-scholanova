package game.modele;

public class Game {
    private Operation[] operations;

    public Game (int maxOperation, int maxNombre) {
        this.operations = new Operation[maxOperation];

        for (int i = 0; i < this.operations.length; i++) {
            this.operations[i] = new Operation(maxNombre);
            this.operations[i].resultat();
        }
    }

    public Operation getOperation (int idx) {
        return this.operations[idx];
    }

    public int getLength () {
        return this.operations.length;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder()
                .append("Game {")
                .append("\n");

        for (Operation operation : operations) {
            stringBuilder.append("     ")
                    .append(operation)
                    .append("   ")
                    .append(operation.getResultat())
                    .append("\n");
        }

        return stringBuilder
                .append("}")
                .toString();
    }
}

package game;

import game.modele.Game;
import game.modele.Operation;

import java.util.Scanner;

public class Main {
    private static Game game;
    private static Scanner scanner;

    private static int score = 0;

    public static void main(String[] args) {
        game = new Game(10, 10);
        scanner = new Scanner(System.in);

        jouer();
    }

    public static void jouer () {
        for (int i = 0; i < 10; i++) {
            Operation operation = game.getOperation(i);
            System.out.println(operation);
            double reponse = scanner.nextDouble();

            if (operation.getResultat().equals(reponse))
                score++;
        }

        System.out.println(score);
    }
}
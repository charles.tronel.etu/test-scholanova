package game;

import game.modele.Game;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.text.DecimalFormat;

public class MainGUI extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("view/game.fxml"));

        stage.setTitle("Test ScholaNova");
        stage.setScene(new Scene(root, 300, 400));

        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}
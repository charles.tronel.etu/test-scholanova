package game.controler;

import game.modele.Game;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

import java.net.URL;
import java.text.DecimalFormat;
import java.util.ResourceBundle;

public class GameController implements Initializable {
    public static int score = 0, idx = 0;
    private static DecimalFormat decimalFormat = new DecimalFormat("0.00");

    private Game game;
    private StringBuilder stringBuilder;

    @FXML
    public Label operation, reponse;
    @FXML
    public Button btn0, btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9, btnValid, btnSupp, btnSous, btnPoint;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.game = new Game(10, 10);
        this.stringBuilder = new StringBuilder();

        this.operation.setText(game.getOperation(idx).toString());

        this.btnValid.setOnMouseClicked(mouseEvent -> {
            if (this.stringBuilder.length() != 0 && idx < this.game.getLength()) {
                if (decimalFormat.format(this.game.getOperation(idx).getResultat())
                        .equals(decimalFormat.format(Double.parseDouble(this.stringBuilder.toString())))) {
                    score++;
                }

                idx++;

                if (idx < this.game.getLength()) {
                    this.stringBuilder.setLength(0);
                    this.reponse.setText(this.stringBuilder.toString());
                    this.operation.setText(this.game.getOperation(idx).toString());
                }
            } else {
                this.operation.setText("Votre score est : ");
                this.reponse.setText(score + "/" + this.game.getLength());
            }
        });

        btnSupp.setOnAction(mouseEvent -> {
            if (this.stringBuilder.length() != 0) {
                this.stringBuilder.deleteCharAt(this.stringBuilder.length() - 1);
                this.reponse.setText(this.stringBuilder.toString());
            }
        });

        this.btnSous.setOnMouseClicked(event -> actionBtnSous());
        this.btnPoint.setOnMouseClicked(event -> actionBtnPoint());

        this.btn0.setOnMouseClicked(event -> actionBtn("0"));
        this.btn1.setOnMouseClicked(event -> actionBtn("1"));
        this.btn2.setOnMouseClicked(event -> actionBtn("2"));
        this.btn3.setOnMouseClicked(event -> actionBtn("3"));
        this.btn4.setOnMouseClicked(event -> actionBtn("4"));
        this.btn5.setOnMouseClicked(event -> actionBtn("5"));
        this.btn6.setOnMouseClicked(event -> actionBtn("6"));
        this.btn7.setOnMouseClicked(event -> actionBtn("7"));
        this.btn8.setOnMouseClicked(event -> actionBtn("8"));
        this.btn9.setOnMouseClicked(event -> actionBtn("9"));
    }

    private void actionBtnPoint() {
        if (this.stringBuilder.length() != 0
                && !this.stringBuilder.toString().contains(".")
                && this.stringBuilder.charAt(0) != '-') {
            this.stringBuilder.append(".");
            this.reponse.setText(this.stringBuilder.toString());
        }
    }

    private void actionBtnSous() {
        if (this.stringBuilder.length() == 0) {
            this.stringBuilder.append("-");
            this.reponse.setText(this.stringBuilder.toString());
        }
    }

    private void actionBtn (String value) {
        this.stringBuilder.append(value);
        this.reponse.setText(this.stringBuilder.toString());
    }

    public void addDropShadow(MouseEvent mouseEvent) {
        ((Button) mouseEvent.getSource()).setEffect(new DropShadow(6, Color.WHITE));
    }

    public void delDropShadow(MouseEvent mouseEvent) {
        ((Button) mouseEvent.getSource()).setEffect(null);
    }
}